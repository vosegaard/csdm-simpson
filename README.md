There are two options to use the CSDM library with SIMPSON.

1. Use the csdm package included like this
   lappend ::auto_path /path/to/tcllib ./csdm-1.0
   package require csdm
2. Source the csdm.in package like this
   source csdm.in

