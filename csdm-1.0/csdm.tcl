package provide csdm 1.0.0

#lappend ::auto_path /System/Library/Tcl/tcllib1.12 ./tcltls-1.7.20
package require json
#package require json::write
package require http 2
#package require units

package require tls 1.7
http::register https 443 [list ::tls::socket -autoservername true]

# Make the SIMPSON spectrum resemble as much as possible the CSDM spectrum
# - same number of dimensions, if possible
# - same spectral width, if possible
# 
# Rewrite
# - fcreate to allow creation of CSDM files
# - fload to allow loading CSDM files
# - fft
# - ftranspose
# - fset, fget, fsetindex, findex
# - fset/fget/fcreate nomenclature is
#   fget $f -dimensions.0.count
#   fset $f -dimensions.1.reciprocal.origin_offset "123 MHz"

rename fsave fsaveinternal
rename fload floadinternal
rename fcreate fcreateinternal
rename fset fsetinternal
rename fget fgetinternal
rename fzerofill fzerofillinternal
rename fft fftinternal
rename fdup fdupinternal
rename fdupzero fdupzerointernal
rename funload funloadinternal
rename fnewnp fnewnpinternal
rename fextract fextractinternal



namespace eval ::csdm {}

proc fsave {ff name args} {
   if {[lsearch $args "-csdf"] >= 0} {
      puts "Old CSDM saving routine"
      puts [concat fsaveoldcsdm [list $ff] $name $args]
      eval [concat fsaveoldcsdm [list $ff] $name $args]
   } elseif {[regexp ".csdf$" $name]} {
      # Save CSDF format, first fix component
      set di $::csdm([lindex $ff 0])
      set dv [dict get $di dependent_variables 0]
      for {set j 0} {$j < [llength $ff]} {incr j} {
         set f [lindex $ff $j]
         if {$j == 0} {
            set nt [dict get $di dependent_variables 0 numeric_type]
            set ntot [fgetinternal $f -np]
            set ni [fgetinternal $f -ni]
            if {$ni > 0} {set ntot [expr $ntot*$ni]}
         } else {
            dict set di dependent_variables $j [dict get $::csdm($f) dependent_variables 0]
         }
         dict set di dependent_variables $j encoding "base64"
         if {[info exists ary]} {unset ary}
         if {[regexp {complex} $nt]} {
            for {set i 1} {$i <= $ntot} {incr i} {
               set c [findex $f $i]
               lappend ary $c
            }
            set ary [join $ary]
            dict set di dependent_variables $j numeric_type "complex64"
         } else {
            for {set i 1} {$i <= $ntot} {incr i} {
               set re [findex $f $i -re]
               lappend ary $re
            }
            dict set di dependent_variables $j numeric_type "float32"
         }
         set b64 [binary encode base64 [binary format "f*" $ary]]
         dict set di dependent_variables $j components 0 $b64
      }
      for {set i 0} {$i < [llength $args] -1} {incr i} {
         if {[regexp "^-text" [lindex $args $i]]} {
            regsub "^-text" [lindex $args $i] {} indx
            if {$indx == ""} {
               set indx 0
            }
            incr i
            set text [lindex $args $i]
            dict set di dependent_variables $indx application plotText $text
         }
      }
      dict set di timestamp [clock format [clock seconds] -gmt 1 -format "%Y-%m-%dT%TZ"]
      dict unset di geographic_coordinate
      dict set di tags {NMR SIMPSON}
      set fp [open $name w]
      puts $fp [stringify $di]
      close $fp
      return
   } elseif {[regexp ".csdfe$" $name]} {
      # Save CSDFE format
      set di $::csdm([lindex $ff 0])
      for {set i 1} {$i < [llength $ff]} {incr i} {
         dict set di dependent_variables $i [dict get $::csdm([lindex $ff $i]) dependent_variables 0]
      }
      for {set i 0} {$i < [llength $args] -1} {incr i} {
         if {[regexp "^-text" [lindex $args $i]]} {
            regsub "^-text" [lindex $args $i] {} indx
            if {$indx == ""} {
               set indx 0
            }
            incr i
            set text [lindex $args $i]
            dict set di dependent_variables $indx application plotText $text
         }
      }
      set fp [open $name w]
      puts $fp [stringify $di]
      close $fp
      return
   } else {
      if {$args != ""} {
         if {$args == "-nmrpipe" || $args == "-binary"} {
            fsaveinternal $ff $name $args
         }
      } else {
         fsaveinternal $ff $name
      }
   }
}

proc fzerofill {f args} {
   eval [concat [list fzerofillinternal $f] $args]
   set np [fgetinternal $f -np]
   dict set ::csdm($f) dimensions 0 count $np
   set ni [fgetinternal $f -ni]
   if {$ni > 1} {
      dict set ::csdm($f) dimensions 1 count $ni
   }
}

proc funload {f} {
   funloadinternal $f
   array unset ::csdm $f
}

proc fdup {f} {
   set g [fdupinternal $f]
   set ::csdm($g) $::csdm($f)
   return $g
}
proc fdupzero {f} {
   set g [fdupzerointernal $f]
   set ::csdm($g) $::csdm($f)
   return $g
}

proc fextract {f from to} {
   fextractinternal $f $from $to
   set np [fgetinternal $f -np]
   set sw [fgetinternal $f -sw]
   set ref [fgetinternal $f -ref]
   dict set ::csdm($f) dimensions 0 count $np
   dict set ::csdm($f) dimensions 0 coordinates_offset "[expr $ref-$sw/2.0] Hz"
}


proc fnewnp {f newnp} {
   set np [dict get $::csdm($f) dimensions 0 count]
   if {$newnp == $np} {return}
   set incr [dict get $::csdm($f) dimensions 0 increment]
   set i [lindex $incr 0]
   set newincr [expr $i*$np/double($newnp)]
   fnewnpinternal $f $newnp
   dict set ::csdm($f) dimensions 0 increment "$newincr [lindex $incr 1]"
   dict set ::csdm($f) dimensions 0 count $newnp
}



proc fft {f args} {
   if {[llength $args] > 0} {
      eval [concat [list fftinternal $f] $args]
   } else {
      fftinternal $f
   }
   set ndim 1
   if {[fgetinternal $f -ni] > 1} {set ndim 2}
   set type [fgetinternal $f -type]
   set q "frequency"
   set qr "time"
   if {$type == "fid"} {
      set q "time"
      set qr "frequency"
   }
   for {set i 0} {$i < $ndim} {incr i} {
      set q "frequency"
      set qr "time"
      if {$type == "fid"} {
         set q "time"
         set qr "frequency"
      }
      set co 0
      set cor 0
      set oo 0
      set oor 0
      set a 0
      set ar 0
      set p 0
      set pr 0
      if {[dict exists $::csdm($f) dimensions $i coordinates_offset]} {
         set co [dict get $::csdm($f) dimensions $i coordinates_offset]
      }
      if {[dict exists $::csdm($f) dimensions $i origin_offset]} {
         set oo [dict get $::csdm($f) dimensions $i origin_offset]
      }
      if {[dict exists $::csdm($f) dimensions $i period]} {
         set p [dict get $::csdm($f) dimensions $i period]
      }
      if {[dict exists $::csdm($f) dimensions $i quantity_name]} {
         set q [dict get $::csdm($f) dimensions $i quantity_name]
      }
      if {[dict exists $::csdm($f) dimensions $i application]} {
         set a [dict get $::csdm($f) dimensions $i application]
      }
      dict unset ::csdm($f) dimensions $i coordinates_offset 
      dict unset ::csdm($f) dimensions $i origin_offset 
      dict unset ::csdm($f) dimensions $i period 
      dict unset ::csdm($f) dimensions $i application
      if {[dict exists $::csdm($f) dimensions $i reciprocal]} {
         if {[dict exists $::csdm($f) dimensions $i reciprocal coordinates_offset]} {
            set cor [dict get $::csdm($f) dimensions $i reciprocal coordinates_offset]
         }
         if {[dict exists $::csdm($f) dimensions $i reciprocal origin_offset]} {
            set oor [dict get $::csdm($f) dimensions $i reciprocal origin_offset]
         }
         if {[dict exists $::csdm($f) dimensions $i reciprocal period]} {
            set pr [dict get $::csdm($f) dimensions $i reciprocal period]
         }
         if {[dict exists $::csdm($f) dimensions $i reciprocal quantity_name]} {
            set qr [dict get $::csdm($f) dimensions $i reciprocal quantity_name]
         }
         if {[dict exists $::csdm($f) dimensions $i reciprocal application]} {
            set ar [dict get $::csdm($f) dimensions $i reciprocal application]
         }
         dict unset ::csdm($f) dimensions $i reciprocal coordinates_offset 
         dict unset ::csdm($f) dimensions $i reciprocal origin_offset 
         dict unset ::csdm($f) dimensions $i reciprocal period 
         dict unset ::csdm($f) dimensions $i reciprocal application 
      }
      set inc [dict get $::csdm($f) dimensions $i increment]
      set val [getvalue $inc]
      set n [dict get $::csdm($f) dimensions $i count]
      if {$qr == "time"} {
         dict set ::csdm($f) dimensions $i increment "[expr 1.0/$val/$n] s"
      } else {
         dict set ::csdm($f) dimensions $i increment "[expr 1.0/$val/$n] Hz"
      }
      dict set ::csdm($f) dimensions $i quantity_name $qr
      dict set ::csdm($f) dimensions $i reciprocal quantity_name $q
      if {$cor != 0} {
         dict set ::csdm($f) dimensions $i coordinates_offset $cor
      }
      if {$oor != 0} {
         dict set ::csdm($f) dimensions $i origin_offset $oor
      }
      if {$pr != 0} {
         dict set ::csdm($f) dimensions $i period $pr
      }
      if {$ar != 0} {
         dict set ::csdm($f) dimensions $i application $ar
      }
      if {$co != 0} {
         dict set ::csdm($f) dimensions $i reciprocal coordinates_offset $co
      }
      if {$oo != 0} {
         dict set ::csdm($f) dimensions $i reciprocal origin_offset $oo
      }
      if {$p != 0} {
         dict set ::csdm($f) dimensions $i reciprocal period $p
      }
      if {$a != 0} {
         dict set ::csdm($f) dimensions $i reciprocal application $p
      }
   }
}


proc isnumeric {value} {
    set value [string trim $value]
    # Use [string is double] to accept Inf and NaN
    if {[string is double -strict $value]} {
        return true
    }
    regsub {^\s*([+-])*0[BOXbox]?0*([^[:space:]]*)\s*$} $value {\1\2} value
    if {[string is double -strict $value]} {
        return true
    }
    return false
}

proc preparevalue {value pre} {
   if {[isnumeric $value]} {
      return "$pre   $value"
   } else {
      return "$pre   \"$value\""
   }
}
proc preparekeyvalue {key value pre} {
   if {[isnumeric $value] && $key != "version"} {
      return "$pre   \"$key\": $value"
   } else {
      return "$pre   \"$key\": \"$value\""
   }
}




proc dict2jsonarray {dictionary pre} {
   dict for {key value} $dictionary {
      if {[catch {dict size $value}]} {
         lappend result [preparevalue $value $pre]
      } else {
         if {[llength $value] == 2 && [llength [lindex $value 1]] == 1} {
            lappend result [preparevalue $value $pre]
         } else {
            set ary 1
            set keys [dict keys $value]
            for {set i 0} {$i < [llength $keys]} {incr i} {
               if {[lindex $keys $i] != $i} {
                  set ary 0
                  break
               }
            }
            if {$ary} {
               lappend result "$pre   [dict2jsonarray $value "$pre   "]"
            } else {
               lappend result "$pre   [dict2json $value "$pre   "]"
            }
         }
      }
   }
   return "\[\n[join $result ",\n"]\n$pre\]"
}
proc write {key val type pre} {
   switch $type {
      ais {
         # Array internal strings
         return "$pre\"$key\": \[\"[join $val "\", \""]\"\]"
      }
      ain {
         # Array internal strings
         return "$pre\"$key\": \[[join $val ,]\]"
      }
      as {
         for {set i 0} {$i < [llength $val]} {incr i} {
            lappend res "$pre   \"[lindex $val $i]\""
         }
         return "$pre\"$key\": \[\n[join $res ,\n]\n$pre\]"
      }
      an {
         for {set i 0} {$i < [llength $val]} {incr i} {
            lappend res "$pre   [lindex $val $i]"
         }
         return "$pre\"$key\": \[\n[join $res ,\n]\n$pre\]"
      }
      o {
         foreach k [dict keys $val] {
            lappend res "$pre   \"$k\": \"[dict get $val $k]\""
         }
         return "$pre\"$key\": \{\n[join $res ,\n]\n$pre\}"
      }
      n -
      b {
         return "$pre\"$key\": $val"
      }
      s {
         return "$pre\"$key\": \"$val\""
      }
   }
}
proc stringify {dictionary} {
   foreach k {{tags ais} \
      {read_only b} \
      {version s} \
      {timestamp s} \
      {geographic_coordinate o} \
      {description s}} {
      if {[dict exists $dictionary [lindex $k 0]]} {
         lappend res [write [lindex $k 0] [dict get $dictionary [lindex $k 0]] [lindex $k 1] "   "]
      }
   }
   if {[dict exists $dictionary application]} {
      lappend res "   \"application\": [dict2json [dict create dk.pastis.csdm [dict get $dictionary application]] "   "]"
   }
   for {set i 0} {$i < [llength [dict keys [dict get $dictionary dimensions]]]} {incr i} {
      foreach k {{type s} \
         {label s} \
         {description s} \
         {count n} \
         {increment s} \
         {origin_offset s} \
         {quantity_name s} \
         {period s} \
         {complex_fft b} \
         {coordinates as} \
         {labels as} \
         {coordinates_offset s}} {
         if {[dict exists $dictionary dimensions $i [lindex $k 0]]} {
            lappend di [write [lindex $k 0] [dict get $dictionary dimensions $i [lindex $k 0]] [lindex $k 1] "         "]
         }
      }
      if {[dict exists $dictionary dimensions $i application]} {
         lappend di "         \"application\": [dict2json [dict create dk.pastis.csdm [dict get $dictionary dimensions $i application]] "         "]"
      }
      if {[dict exists $dictionary dimensions $i reciprocal]} {
         foreach kk {{label s} \
            {description s} \
            {quantity_name s} \
            {origin_offset s} \
            {period s} \
            {coordinates_offset s}} {
            if {[dict exists $dictionary dimensions $i reciprocal [lindex $kk 0]]} {
               lappend r [write [lindex $kk 0] [dict get $dictionary dimensions $i reciprocal [lindex $kk 0]] [lindex $kk 1] "            "]
            }
         }
         if {[dict exists $dictionary dimensions $i reciprocal application]} {
            lappend r "            \"application\": [dict2json [dict create dk.pastis.csdm [dict get $dictionary dimensions $i reciprocal application]] "            "]"
         }
         if {[info exists r]} {
	         lappend di "         \"reciprocal\": \{\n[join $r ,\n]\n         \}"
	         unset r
         }
      }
      lappend dim "      \{\n[join $di ,\n]\n      \}"
      unset di
   }
   lappend res "   \"dimensions\": \[\n[join $dim ,\n]\n   \]"

   set dd [dict get $dictionary dependent_variables]
   for {set i 0} {$i < [llength [dict keys $dd]]} {incr i} {
      foreach k {{type s} \
         {label s} \
         {description s} \
         {name s} \
         {numeric_type s} \
         {unit s} \
         {quantity_name s} \
         {quantity_type s} \
         {components_url s} \
         {encoding s} \
         {component_labels as}} {
         if {[dict exists $dd $i [lindex $k 0]]} {
            lappend di [write [lindex $k 0] [dict get $dd $i [lindex $k 0]] [lindex $k 1] "         "]
         }
      }
      if {[dict exists $dictionary dependent_variables $i application]} {
         lappend di "         \"application\": [dict2json [dict create dk.pastis.csdm [dict get $dictionary dependent_variables $i application]] "         "]"
      }
      if {[dict get $dd $i type] == "internal"} {
         if {[dict get $dd $i encoding] == "base64"} {
            lappend di "         \"components\": \[\n            \"[dict get $dd $i components 0]\"\n         \]"
         } else {
            puts stderr "Unable to write other encoding"
            exit
         }
      }
      if {[dict exists $dd $i sparse_sampling]} {
         foreach kk {{dimension_indexes ain} \
            {encoding s} \
            {unsigned_integer_type s}} {
            if {[dict exists $dd $i sparse_sampling [lindex $kk 0]]} {
               lappend s [write [lindex $kk 0] [dict get $dd $i sparse_sampling [lindex $kk 0]] [lindex $kk 1] "            "]
            }
         }
         if {[dict get $dd $i sparse_sampling encoding] == "base64"} {
            lappend s "            \"sparse_grid_vertexes\": \"[dict get $dd $i sparse_sampling sparse_grid_vertexes]\""
         } else {
              set x [dict get $dd $i sparse_sampling sparse_grid_vertexes]
              lappend s "            \"sparse_grid_vertexes\": \[[join $x ,]\]"
         }
         lappend di "         \"sparse_sampling\": \{\n[join $s ,\n]\n         \}"
         unset s
      }
      lappend dv "      \{\n[join $di ,\n]\n      \}"
      unset di
   }
   lappend res "   \"dependent_variables\": \[\n[join $dv ,\n]\n   \]"
   return "\{\n   \"csdm\": \{\n[join $res ,\n]\n   \}\n\}"
}




proc dict2json {dictionary pre} {
   dict for {key value} $dictionary {
      if {[catch {dict size $value}]} {
         lappend result [preparekeyvalue $key $value $pre]
      } else {
         if {[llength $value] == 2 && [llength [lindex $value 1]] == 1 && $key != "components"} {
            lappend result [preparekeyvalue $key $value $pre]
         } else {
            set ary 1
            set keys [lsort [dict keys $value]]
            for {set i 0} {$i < [llength $keys]} {incr i} {
               if {[lindex $keys $i] != $i} {
                  set ary 0
                  break
               }
            }
            if {[llength $value] == 0} {continue}
            if {$ary} {
               lappend result "$pre   \"$key\": [dict2jsonarray $value "$pre   "]"
            } else {
               lappend result "$pre   \"$key\": [dict2json $value "$pre   "]"
            }
         }
      }
   }
   return "\{\n[join $result ",\n"]\n$pre\}"
}

proc fsimpson {{fitpar {}}} {
   global par _fd

   if [info exists par(verbose)] {
      if {[string index $par(verbose) 0] == 1}  {
         puts "Parameters"
         foreach k [lsort [array names par]] {
            puts [format "  %-20s %s" $k $par($k)]
         }
      }
   }
   spinsys_resolve $fitpar
   set par(tcalc) [lindex [time {uplevel #0 {set _fd [internalsimpson]}}] 0]
   set f $_fd
   unset _fd
  
   # Modifications here to comply with CSDM
  
   set d [dict create version "1.0" dimensions {0 {type linear}} dependent_variables {0 {type internal numeric_type complex64 quantity_type scalar encoding base64}}]
   foreach i [array names par] {
      dict set d dependent_variables 0 application par $i $par($i)
   }
   if {[info exists par(spin_rate)]} {
      dict set d dependent_variables 0 application spin_rate "$par(spin_rate) Hz"
   }
   if [info exists par(proton_frequency)] {
      dict set d dependent_variables 0 application larmor_frequency "$par(proton_frequency) Hz"
   }
   dict set d dependent_variables 0 application arguments $::argv
   dict set d dependent_variables 0 name [lindex $::argv 0]
   set fp [open [lindex $::argv 0] r]
   set data [read $fp]
   close $fp
   dict set d dependent_variables 0 application input_file [binary encode base64 $data]
   dict set d dimensions 0 count $par(np)
   if [info exists par(proton_frequency)] {
      set first [string first I $par(detect_operator)]
      set det [string range $par(detect_operator) $first [expr $first+2]]
      regsub -all {[Ixyzpmcab]*} $det {} num
      if {$num == n} {
         set num 1
      }
      set nuc [lindex $::spinsys(nuclei) [expr $num-1]]
      set freq [resfreq $nuc $par(proton_frequency)]
   }
   if {[info exists par(sw)]} {
      if {[fgetinternal $f -type] == "spe"} {
         dict set d dimensions 0 increment "[expr $par(sw)/double($par(np))] Hz"
         dict set d dimensions 0 quantity_name "frequency"
         dict set d dimensions 0 reciprocal quantity_name "time"
         set ref [expr -$par(sw)/2.0]
         if [info exists par(ref)] {
            set ref [expr $ref + $par(ref)]
         }
         dict set d dimensions 0 coordinates_offset "$ref Hz"
         if [info exists freq] {dict set d dimensions 0 origin_offset "$freq Hz"}
      } else {
         dict set d dimensions 0 increment "[expr 1.0/$par(sw)] s"
         dict set d dimensions 0 quantity_name "time"
         dict set d dimensions 0 reciprocal quantity_name "frequency"
         set ref [expr -$par(sw)/2.0]
         if [info exists par(ref)] {
            set ref [expr $ref + $par(ref)]
         }
         dict set d dimensions 0 reciprocal coordinates_offset "$ref Hz"
         if [info exists freq] {dict set d dimensions 0 reciprocal origin_offset "$freq Hz"}
      }
      if {[info exists par(ni)]} {
         if {$par(ni) > 1} {
            dict set d dimensions 1 [dict create type linear count $par(ni)]
            if {[fgetinternal $f -type] == "spe"} {
               dict set d dimensions 1 increment "[expr $par(sw1)/double($par(ni))] Hz"
               dict set d dimensions 1 quantity_name "frequency"
               dict set d dimensions 1 reciprocal quantity_name "time"
               set ref [expr -$par(sw1)/2.0]
               if [info exists par(ref1)] {
                  set ref [expr $ref + $par(ref1)]
               }
               dict set d dimensions 1 coordinates_offset "$ref Hz"
               if [info exists freq] {dict set d dimensions 1 origin_offset "$freq Hz"}
            } else {
               dict set d dimensions 1 increment "[expr 1.0/$par(sw1)] s"
               dict set d dimensions 1 quantity_name "time"
               dict set d dimensions 1 reciprocal quantity_name "frequency"
               set ref [expr -$par(sw1)/2.0]
               if [info exists par(ref1)] {
                  set ref [expr $ref + $par(ref1)]
               }
               dict set d dimensions 1 reciprocal coordinates_offset "$ref Hz"
               if [info exists freq] {dict set d dimensions 1 reciprocal origin_offset "$freq Hz"}
            }
         }
      }
   }
   set ::csdm($f) $d
   return $f
}

proc fcreate {args} {
   set d [dict create version "1.0" dimensions {} dependent_variables {0 {type internal numeric_type complex64 quantity_type scalar}}]
   set csdm true
   for {set i 0} {$i < [llength $args]-1} {incr i} {
      if {[lindex $args $i] == "-np"} {
         set csdm false
         break
      }
   }
   set ref 0
   if {$csdm} {
      # Create a spectrum using CSDM specification
      for {set i 0} {$i < [llength $args]-1} {incr i} {
         regsub -- "^-" [lindex $args $i] {} tmp
         set ary [split $tmp .]
         incr i
         set val [lindex $args $i]
         set n [llength $ary]
         if {[lindex $ary end] == "coordinates"} {
            # Fix monotonic dimension
            if {[info exists cary]} {unset cary}
            for {set j 0} {$j < [llength $val]} {incr j} {
               lappend cary "\"[lindex $val $j]\""
            }
            set val "{[join $cary]}"
         }
         set cmd [concat {dict set d} $ary $val]
         eval $cmd
      }
      set ndim [llength [dict keys [dict get $d dimensions]]]
      set ndep [llength [dict keys [dict get $d dependent_variables]]]
      if {[dict get $d dimensions 0 quantity_name] == "time"} {
         set type "fid"
      } else {
         set type "spe"
      }
      if {$ndep != 1} {
         puts stderr "Sorry, cannot handle more than one set of dependent_variables"
         exit
      }
      if {[dict get $d dependent_variables 0 quantity_type] != "scalar"} {
         puts stderr "Sorry, cannot handle other quantity_types than scalar"
         exit
      }
      if {$ndim > 2} {
         puts stderr "We can try to handle a ${ndim}D dataset, but may run into problems"
      }
      if {![dict exists $d dimensions 0 count]} {
         puts stderr "Specify count for dimension 0"
         exit
      }
      if {![dict exists $d dimensions 0 type]} {
         puts stderr "Specify type (linear, monotonic, labeled) for dimension 0"
         exit
      }
      set t [dict get $d dimensions 0 type]
      if {$t == "linear"} {
         set np [dict get $d dimensions 0 count]
         set inc [getvalue [dict get $d dimensions 0 increment]]
         if {[dict get $d dimensions 0 quantity_name] == "time"} {
            set sw [expr 1.0/$inc]
         } else {
            set sw [expr $inc*$np]
            set co [expr -$sw/2.0]
            if {[dict exists $d dimensions 0 coordinates_offset]} {
               set co [getvalue [dict get $d dimensions 0 increment]]
            }
            set ref [expr $co + $sw/2.0]
         }
      } elseif {$t == "monotonic"} {
         set np [llength [dict get $d dimensions 0 coordinates]]
         set sw 1
      } elseif {$t == "labeled"} {
         set np [llength [dict get $d dimensions 0 labels]]
         set sw 1
      } else {
         puts stderr "Unknown dimension type $t"
         exit
      }
      if {$ndim == 2} {
         set t [dict get $d dimensions 1 type]
         if {$t == "linear"} {
            set ni [dict get $d dimensions 1 count]
            set inc [getvalue [dict get $d dimensions 1 increment]]
            if {[dict get $d dimensions 1 quantity_name] == "time"} {
               set sw1 [expr 1.0/$inc]
            } else {
               set sw1 [expr $inc*$ni]
               set co [expr -$sw1/2.0]
               if {[dict exists $d dimensions 1 coordinates_offset]} {
                  set co [getvalue [dict get $d dimensions 1 coordinates_offset]]
               }
               set ref1 [expr $co + $sw1/2.0]
            }
         } elseif {$t == "monotonic"} {
            set ni [llength [dict get $d dimensions 1 coordinates]]
            set sw1 1
         } elseif {$t == "labeled"} {
            set ni [llength [dict get $d dimensions 1 labels]]
            set sw1 1
         } else {
            puts stderr "Unknown dimension type $t"
            exit
         }
      }
   } else {
      # Create a spectrum using the conventional arguments
      for {set i 0} {$i < [llength $args]-1} {incr i} {
         set cmd [lindex $args $i]
         incr i
         set val [lindex $args $i]
         if {$cmd == "-np"} {
            set np $val
            dict set d dimensions 0 count $np
            dict set d dimensions 0 type "linear"
         } elseif {$cmd == "-sw"} {
            set sw $val
         } elseif {$cmd == "-ref"} {
            set ref $val
         } elseif {$cmd == "-ni"} {
            set ni $val
            dict set d dimensions 1 count $ni
            dict set d dimensions 1 type "linear"
         } elseif {$cmd == "-sw1"} {
            set sw1 $val
         } elseif {$cmd == "-ref1"} {
            set ref1 $val
         } elseif {$cmd == "-type"} {
            set type $val
         }
      }
      set ndim [llength [dict keys [dict get $d dimensions]]]
      if {$type == "fid"} {
         dict set d dimensions 0 increment "[expr 1.0/$sw] s"
         dict set d dimensions 0 quantity_name "time"
         if {[info exists ref]} {
            dict set d dimensions 0 reciprocal coordinates_offset "[expr $ref-$sw/2] Hz"
         }
         if {[info exists ni]} {
            dict set d dimensions 1 increment "[expr 1.0/$sw1] s"
            if {[info exists ref1]} {
               dict set d dimensions 1 reciprocal coordinates_offset "[expr $ref1-$sw1/2] Hz"
            }
         }
      } else {
         dict set d dimensions 0 increment "[expr $sw/double($np)] Hz"
         dict set d dimensions 0 quantity_name "frequency"
         if {[info exists ref]} {
            dict set d dimensions 0 coordinates_offset "[expr $ref - $sw/2.0] Hz"
         } else {
            dict set d dimensions 0 coordinates_offset "[expr -$sw/2.0] Hz"
         }
         if {[info exists ni]} {
            dict set d dimensions 1 increment "[expr $sw1/double($ni)] Hz"
            if {[info exists ref1]} {
               dict set d dimensions 1 coordinates_offset "[expr $ref1 - $sw1/2.0] Hz"
            } else {
               dict set d dimensions 1 coordinates_offset "[expr -$sw1/2.0] Hz"
            }
         }
      }
   }
   set cmd [list fcreateinternal -type $type -np $np -sw $sw -ref $ref]
   if {[info exists ni]} {
      if {$ndim > 2} {
         set ni 1
         for {set i 1} {$i < $ndim} {incr i} {
            set t [dict get $d dimensions $i type]
            if {$t == "linear"} {
               set n [dict get $d dimensions $i count]
            } elseif {$t == "monotonic"} {
               set n [llength [dict get $d dimensions $i coordinates]]
            } elseif {$t == "labeled"} {
               set n [llength [dict get $d dimensions $i labels]]
            }
            set ni [expr $ni*$n]
         }
         set sw1 1
      }
      if {[info exists ref1]} {
         set cmd [concat $cmd [list -ni $ni -sw1 $sw1 -ref1 $ref1]]
      } else {
         set cmd [concat $cmd [list -ni $ni -sw1 $sw1]]
      }
   }
   set f [eval $cmd]
   set ::csdm($f) $d
   return $f
}



proc fset {f args} {
   set refchanged 0
   set swchanged 0
   for {set i 0} {$i < [llength $args]-1} {incr i} {
      set opt [lindex $args $i]
      incr i
      set val [lindex $args $i]
      if {$opt == "-sw"} {
         fsetinternal $f -sw $val
         if {[dict get $::csdm($f) dimensions 0 type] == "linear"} {
            set unit "Hz"
            set inc [expr $val/double([fgetinternal $f -np])]
            if {[dict exists $::csdm($f) dimensions 0 quantity_name]} {
               if {[dict get $::csdm($f) dimensions 0 quantity_name] == "time"} {
                  set unit "s"
                  set inc [expr 1.0/$val]
               }
            }
            dict set ::csdm($f) dimensions 0 increment "$val $unit"
         }
      } elseif {$opt == "-ref"} {
         fsetinternal $f -ref $val
         set t [dict get $::csdm($f) dimensions 0 type]
         if {$t == "linear"} {
            set sw [fget $f -sw]
            if {[dict exists $::csdm($f) dimensions 0 quantity_name]} {
               if {[dict get $::csdm($f) dimensions 0 quantity_name] == "time"} {
                  dict set ::csdm($f) dimensions 0 reciprocal coordinates_offset "[expr $val - $sw/2.0] Hz"
               } else {
                  dict set ::csdm($f) dimensions 0 coordinates_offset "[expr $val - $sw/2.0] Hz"
               }
            } else {
               dict set ::csdm($f) dimensions 0 coordinates_offset "[expr $val - $sw/2.0] Hz"
            }
         }
      } elseif {$opt == "-sw1"} {
         fsetinternal $f -sw1 $val
         if {[dict get $::csdm($f) dimensions 1 type] == "linear"} {
            set unit "Hz"
            set inc [expr $val/double([fgetinternal $f -ni])]
            if {[dict exists $::csdm($f) dimensions 1 quantity_name]} {
               if {[dict get $::csdm($f) dimensions 1 quantity_name] == "time"} {
                  set unit "s"
                  set inc [expr 1.0/$val]
               }
            }
            dict set ::csdm($f) dimensions 1 increment "$val $unit"
         }
      } elseif {$opt == "-ref1"} {
         fsetinternal $f -ref1 $val
         set t [dict get $::csdm($f) dimensions 1 type]
         if {$t == "linear"} {
            set sw [fget $f -sw1]
            if {[dict exists $::csdm($f) dimensions 1 quantity_name]} {
               if {[dict get $::csdm($f) dimensions 1 quantity_name] == "time"} {
                  dict set ::csdm($f) dimensions 1 reciprocal coordinates_offset "[expr $val - $sw/2.0] Hz"
               } else {
                  dict set ::csdm($f) dimensions 1 coordinates_offset "[expr $val - $sw/2.0] Hz"
               }
            } else {
               dict set ::csdm($f) dimensions 1 coordinates_offset "[expr $val - $sw/2.0] Hz"
            }
         }
      } elseif {$opt == "-type"} {
         fset $f -type $val
         set q "frequency"
         set qr "time"
         if {$val == "fid"} {
            set q "time"
            set qr "frequency"
         }
         set ndim [llength [dict get $::csdm($f) dimensions]]
         for {set i 0} {$i < $ndim} {incr i} {
            dict set ::csdm($f) dimensions $i quantity_name $q
            dict set ::csdm($f) dimensions $i reciprocal quantity_name $qr
         }
      } else {
         set ary [split [string trimleft $opt "-"] "."]
         #puts [concat [list dict set ::csdm($f)] $ary $val]]
         eval [concat [list dict set ::csdm($f)] $ary [list $val]]
         # Check if sw is changed...
         set cmd [lindex $ary end]
         set reciprocal [lindex $ary end-1]
         set dim [lindex $ary 1]
         if {$cmd == "coordinates_offset"} {
            set refchanged 1
         } elseif {$cmd == "increment"} {
            set swchanged 1
         }
      }
   }
   if {$swchanged || $refchanged} {
      set ndim [llength [dict keys [dict get $::csdm($f) dimensions]]]
      if {[dict get $::csdm($f) dimensions 0 quantity_name] == "time"} {
         set type "fid"
      } else {
         set type "spe"
      }
      set t [dict get $::csdm($f) dimensions 0 type]
      if {$t == "linear"} {
         set np [dict get $::csdm($f) dimensions 0 count]
         set inc [getvalue [dict get $::csdm($f) dimensions 0 increment]]
         if {[dict get $::csdm($f) dimensions 0 quantity_name] == "time"} {
            set sw [expr 1.0/$inc]
         } else {
            set sw [expr $inc*$np]
            set co [expr -$sw/2.0]
            if {[dict exists $::csdm($f) dimensions 0 coordinates_offset]} {
               set co [getvalue [dict get $::csdm($f) dimensions 0 coordinates_offset]]
            }
            set ref [expr $co + $sw/2.0]
         }
         fsetinternal $f -sw $sw -ref $ref
      }
      if {$ndim == 2} {
         set t [dict get $::csdm($f) dimensions 1 type]
         if {$t == "linear"} {
            set ni [dict get $::csdm($f) dimensions 1 count]
            set inc [getvalue [dict get $::csdm($f) dimensions 1 increment]]
            if {[dict get $::csdm($f) dimensions 1 quantity_name] == "time"} {
               set sw1 [expr 1.0/$inc]
            } else {
               set sw1 [expr $inc*$ni]
               set co [expr -$sw1/2.0]
               if {[dict exists $::csdm($f) dimensions 1 coordinates_offset]} {
                  set co [getvalue [dict get $::csdm($f) dimensions 1 coordinates_offset]]
               }
               #puts $co
               set ref1 [expr $co + $sw1/2.0]
            }
            fsetinternal $f -sw1 $sw1 -ref1 $ref1
         }
      }
   }
}


proc fget {f opt} {
   if {$opt == "-np"} {
      return [fgetinternal $f -np]
   } elseif {$opt == "-sw"} {
      return [fgetinternal $f -sw]
   } elseif {$opt == "-ref"} {
      return [fgetinternal $f -ref]
   } elseif {$opt == "-type"} {
      return [fgetinternal $f -type]
   } elseif {$opt == "-sw1"} {
      return [fgetinternal $f -sw1]
   } elseif {$opt == "-ni"} {
      return [fgetinternal $f -ni]
   } elseif {$opt == "-ref1"} {
      return [fgetinternal $f -ref1]
   }
   set ary [split [string trimleft $opt "-"] "."]
   return [eval [concat [list dict get $::csdm($f)] $ary]]
}

proc getvalue {val} {
   set u [lindex $val 1]
   set mul 1.0
   if {[string index $u [expr [string length $u]-1]] == "s"} {set type "fid"}
   if {[string length $u] > 1} {
      regsub {µ} $u {u} u
      set p [string index $u 0]
      switch $p {
         n {set mul 1.0e-9}
         u {set mul 1.0e-6}
         m {set mul 1.0e-3}
         k {set mul 1.0e3}
         M {set mul 1.0e6}
         G {set mul 1.0e9}
      }
   }
   return [expr [lindex $val 0] * $mul]
}

proc readDimension {di indx} {
   set t [dict get $di dimensions $indx type]
   set type spe
   if {$t == "linear"} {
      set np [dict get $di dimensions $indx count]
      if {[dict exists $di dimensions $indx quantity_name]} {
         set q [dict get $di dimensions $indx quantity_name]
         if {$q == "time"} {
            set type "fid"
         }
      }
      set inc [dict get $di dimensions $indx increment]
      set u [lindex $inc 1]
      if {[string index $u [expr [string length $u]-1]] == "s"} {set type "fid"}
      set ii [getvalue $inc]
      if {$type == "fid"} {
         set sw [expr 1.0/$ii]
         set off [expr -$sw/2.0]
         if {[dict exists $di dimensions $indx reciprocal coordinates_offset]} {
            set off [getvalue [dict get $di dimensions $indx reciprocal coordinates_offset]]
         }
         set ref [expr $off + $sw/2.0]
      } else {
         set sw [expr $ii*$np]
         set off [expr -$sw/2.0]
         if {[dict exists $di dimensions $indx coordinates_offset]} {
            set off [getvalue [dict get $di dimensions $indx coordinates_offset]]
         }
         set ref [expr $off + $sw/2.0]
      }
   } elseif {$t == "monotonic"} {
      set c [dict get $di dimensions $indx coordinates]
      set np [llength $c]
      set sw 1
      set ref 0
   } elseif {$t == "labeled"} {
      set c [dict get $di dimensions $indx labels]
      set np [llength $c]
      set sw 1
      set ref 0
   }
   return [list $type $np $sw $ref]
}

proc fload {name {id 0}} {
   if {[regexp {.csdf} $name]} {
      set fp [open $name r]
      set data [read $fp]
      close $fp
      set di [dict get [::json::json2dict $data] csdm]
      foreach k {"dimensions" "dependent_variables"} {
         set val [dict get $di $k]
         dict unset di $k
         for {set i 0} {$i < [llength $val]} {incr i} {
            dict set di $k $i [lindex $val $i]
         }
      }
      set ndim [llength [dict keys [dict get $di dimensions]]]
      set dim [readDimension $di 0]
      set type [lindex $dim 0]
      set np [lindex $dim 1]
      set sw [lindex $dim 2]
      set ref [lindex $dim 3]
      if {$ndim == 1} {
         set f [fcreateinternal -type $type -np $np -sw $sw -ref $ref]
      } elseif {$ndim == 2} {
         set dim [readDimension $di 1]
         set f [fcreateinternal -type $type -np $np -sw $sw -ref $ref -ni [lindex $dim 1] -sw1 [lindex $dim 2] -ref1 [lindex $dim 3]]
      } else {
         set ni 1
         for {set i 1} {$i < $ndim} {incr i} {
            set dim [readDimension $di $i]
            set ni [expr $ni*[lindex $dim 1]]
         }
         set f [fcreateinternal -type $type -np $np -sw $sw -ref $ref -ni $ni -sw1 1]
      }
      set dt [dict get $di dependent_variables $id type]
      set trans [dict create float32 "f*" complex64 "f*" float64 "d*" complex128 "d*"]
      if {$dt == "internal"} {
         set component [lindex [dict get $di dependent_variables $id components] 0]
         set enc ""
         if {[dict exists $di dependent_variables 0 encoding]} {
            set enc [dict get $di dependent_variables 0 encoding]
         }
         set nt [dict get $di dependent_variables $id numeric_type]
         set complex [regexp "complex" $nt]
         if {$enc == "base64"} {
            set form [dict get $trans $nt]
            binary scan [binary decode base64 $component] $form ary
         } else {
            set ary $component
         }
         set ntot [fgetinternal $f -np]
         set ni [fgetinternal $f -ni]
         if {$ni > 0} {set ntot [expr $ntot*$ni]}
         set ii 1
         set im 0
         for {set i 0} {$i < [llength $ary]} {} {
            set re [lindex $ary $i]
            incr i
            if {$complex} {
               set im [lindex $ary $i]
               incr i
            }
            fsetindex $f $ii $re $im
            incr ii
         }
         dict unset di dependent_variables $id components
      } else {
         set url [dict get $di dependent_variables $id components_url]
         if {[regexp {^file:} $url]} {
            puts "Read external data from file $url"
            regsub {^file:} $url "" fname
            set fp [open $fname r]
            fconfigure $fp -encoding binary -translation binary
            set data [read $fp]
            close $fp
         } else {
            puts "Read external data from url $url"
            set token [::http::geturl $url]
            ::http::wait $token
            set data [::http::data $token]
         }
         set nt [dict get $di dependent_variables $id numeric_type]
         set complex [regexp "complex" $nt]
         set form [dict get $trans $nt]
         binary scan $data $form ary
         set ntot [fgetinternal $f -np]
         set ni [fgetinternal $f -ni]
         if {$ni > 0} {set ntot [expr $ntot*$ni]}
         set ii 1
         puts "$ntot [llength $ary]"
         set im 0
         for {set i 0} {$i < [llength $ary]} {} {
            set re [lindex $ary $i]
            incr i
            if {$complex} {
               set im [lindex $ary $i]
               incr i
            }
            fsetindex $f $ii $re $im
            incr ii
         }
      }
      set dv [dict get $di dependent_variables $id]
      dict unset di dependent_variables
      dict set di dependent_variables 0 $dv
   } else {
      if {$id == "-nmrpipe"} {
         set f [floadinternal $name -nmrpipe]
      } else {
         set f [floadinternal $name]
      }
      set di [dict create version "1.0" dimensions {0 {type linear}} dependent_variables {0 {type internal numeric_type complex64 quantity_type scalar encoding base64}}]
      set np [fgetinternal $f -np]
      dict set di dimensions 0 count $np
      if {[fgetinternal $f -type] == "fid"} {
         dict set di dimensions 0 quantity_name "time"
         dict set di dimensions 0 increment "[expr 1.0/[fgetinternal $f -sw]] s"
         dict set di dimensions 0 reciprocal coordinates_offset "[expr [fgetinternal $f -ref]-[fgetinternal $f -sw]/2.0] Hz"
      } else {
         dict set di dimensions 0 quantity_name "frequency"
         dict set di dimensions 0 increment "[expr [fgetinternal $f -sw]/double($np)] Hz"
         dict set di dimensions 0 coordinates_offset "[expr [fgetinternal $f -ref]-[fgetinternal $f -sw]/2.0] Hz"
      }
      if {[fgetinternal $f -ni] > 1} {
         set np [fget $f -ni]
         dict set di dimensions 1 type linear
         dict set di dimensions 1 count $np
         if {[fgetinternal $f -type] == "fid"} {
            dict set di dimensions 1 quantity_name "time"
            dict set di dimensions 1 increment "[expr 1.0/[fgetinternal $f -sw1]] s"
            dict set di dimensions 1 reciprocal coordinates_offset "[expr [fgetinternal $f -ref1]-[fgetinternal $f -sw1]/2.0] Hz"
         } else {
            dict set di dimensions 1 quantity_name "frequency"
            dict set di dimensions 1 increment "[expr [fgetinternal $f -sw1]/double($np)] Hz"
            dict set di dimensions 1 coordinates_offset "[expr [fgetinternal $f -ref1]-[fgetinternal $f -sw1]/2.0] Hz"
         }
      }
   }
   if {[dict exists $di application dk.pastis.csdm]} {
      set app [dict get $di application dk.pastis.csdm]
      dict unset di application
      dict set di application $app
   } else {
      dict unset di application
   }
   for {set i 0} {$i < [llength [dict keys [dict get $di dimensions]]]} {incr i} {
      if {[dict exists $di dimensions $i application dk.pastis.csdm]} {
         set app [dict get $di dimensions $i application dk.pastis.csdm]
         dict unset di dimensions $i application
         dict set di dimensions $i application $app
      } else {
         dict unset di dimensions $i application
      }
      if {[dict exists $di dimensions $i reciprocal]} {
         if {[dict exists $di dimensions $i reciprocal application dk.pastis.csdm]} {
            set app [dict get $di dimensions $i reciprocal application dk.pastis.csdm]
            dict unset di dimensions $i reciprocal application
            dict set di dimensions $i reciprocal application $app
         } else {
            dict unset di dimensions $i reciprocal application
         }
      }
   }
   for {set i 0} {$i < [llength [dict keys [dict get $di dependent_variables]]]} {incr i} {
      if {[dict exists $di dependent_variables $i application dk.pastis.csdm]} {
         set app [dict get $di dependent_variables $i application dk.pastis.csdm]
         dict unset di dependent_variables $i application
         dict set di dependent_variables $i application $app
      } else {
         dict unset di dependent_variables $i application
      }
      if {![dict exists di dependent_variables $i name]} {
         dict set $di dependent_variables $i name $name
      }
   }
   set ::csdm($f) $di
   return $f
}

proc fsaveoldcsdm {ff name args} {
   set type ""
   set real 0
   set sfrq 0
   set sfrq1 0
   set names 0
   set text ""
   set stype [fgetinternal [lindex $ff 0] -type]
   set stype1 $type
   for {set i 0} {$i < [llength $args]} {incr i} {
      if {[lindex $args $i] == "-csdf"} {
         set type "csdf"
      } elseif {[lindex $args $i] == "-binary"} {
         set type "binary"
      } elseif {[lindex $args $i] == "-real"} {
         set real 1
      } elseif {[lindex $args $i] == "-sfrq" && $i < [llength $args]-1} {
         incr i
         set sfrq [lindex $args $i]
         if {$sfrq > 1.0e6} {
            set sfrq [expr $sfrq/1.0e6]
         }
      } elseif {[lindex $args $i] == "-sfrq1" && $i < [llength $args]-1} {
         incr i
         set sfrq1 [lindex $args $i]
         if {$sfrq1 > 1.0e6} {
            set sfrq1 [expr $sfrq1/1.0e6]
         }
      } elseif {[lindex $args $i] == "-type" && $i < [llength $args]-1} {
         incr i
         set types [lindex $args $i]
         set stype [lindex $types 0]
         if {[llength $types] > 1} {set stype1 [lindex $types 1]}
      } elseif {[lindex $args $i] == "-name" && $i < [llength $args]-1} {
         incr i
         set names [lindex $args $i]
      } elseif {[lindex $args $i] == "-text" && $i < [llength $args]-1} {
         incr i
         set text [lindex $args $i]
      }
   }
   if {$type == "csdf"} {
      set f [lindex $ff 0]
      set np [fgetinternal $f -np]
      set ni [fgetinternal $f -ni]
      set npp $np
      if {$ni > 0} {
         set npp [expr $np*$ni]
      }
      set fp [open $name w]
      set ind "      "
      puts $fp "\{\n   \"csdm\": \{"
      puts $fp "$ind\"version\": \"1.0\","
      # Dimensions
      puts -nonewline $fp "$ind\"dimensions\": \["
      set ind "         "
      puts $fp "\{\n$ind\"type\": \"linear\","
      if {$stype == "fid"} {
         puts $fp "$ind\"increment\": \"[expr 1.0/[fgetinternal $f -sw]] s\","
         puts $fp "$ind\"quantity_name\": \"time\","
         if {[fgetinternal $f -ref] != 0} {
            puts $fp "$ind\"coordinates_offset\": \"[fgetinternal $f -ref] s\","
         }
      } else {
         puts $fp "$ind\"coordinates_offset\": \"[expr [fgetinternal $f -ref]-[fgetinternal $f -sw]/2] Hz\","
         if {$sfrq != 0} {
            puts $fp "$ind\"origin_offset\": \"$sfrq MHz\","
         }
         puts $fp "$ind\"increment\": \"[expr [fgetinternal $f -sw]/$np] Hz\","
         puts $fp "$ind\"quantity_name\": \"frequency\","
      }
      puts -nonewline $fp "$ind\"count\": $np\n      \}"
      
      if {$ni > 1} {
         puts $fp ",\{\n$ind\"type\": \"linear\","
         if {$stype1 == "fid"} {
            puts $fp "$ind\"increment\": \"[expr 1.0/[fgetinternal $f -sw1]] s\","
            puts $fp "$ind\"quantity_name\": \"time\","
            if {[fgetinternal $f -ref1] != 0} {
               puts $fp "$ind\"coordinates_offset\": \"[fgetinternal $f -ref1] s\","
            }
         } else {
            puts $fp "$ind\"coordinates_offset\": \"[expr [fgetinternal $f -ref1]-[fgetinternal $f -sw1]/2] Hz\","
            if {$sfrq1 != 0} {
               puts $fp "$ind\"origin_offset\": \"$sfrq1 MHz\","
            }
            puts $fp "$ind\"increment\": \"[expr [fgetinternal $f -sw1]/$ni] Hz\","
            puts $fp "$ind\"quantity_name\": \"frequency\","
         }
         puts -nonewline $fp "$ind\"count\": $ni\n      \}"
      }
      puts $fp "\],"

      # Dependent variables
      puts -nonewline $fp "      \"dependent_variables\": \["
      for {set i 0} {$i < [llength $ff]} {incr i} {
         puts $fp "\{\n$ind\"quantity_type\": \"scalar\","
         puts $fp "$ind\"type\": \"internal\","
         if {$real} {
            puts $fp "$ind\"numeric_type\": \"float32\","
         } else {
            puts $fp "$ind\"numeric_type\": \"complex64\","
         }
         if {[info exists data]} {
            unset data
         }
         set f [lindex $ff $i]
         for {set j 1} {$j <= $npp} {incr j} {
            lappend data [findex $f $j -re]
            if {!$real} {
               lappend data [findex $f $j -im]
            }
         }
         puts $fp "$ind\"components\": \[\n$ind   \"[binary encode base64 [binary format f* $data]]\"\n$ind\],"
         if {$names != 0} {
            puts $fp "$ind\"name\": \"[lindex $names $i]\","
         }
         puts $fp "$ind\"encoding\": \"base64\""
      if {$text != ""} {
         puts "SAVING TEXT $text"
         puts -nonewline $fp ",\n      \"application\": \{\"dk.pastis.easy\": \{\"plotText\": \"$text\"\}\}"
      }
         puts -nonewline $fp "      \}"
         if {$i < [llength $ff]-1} {
            puts -nonewline $fp ","
         }
      }
      puts $fp "\]\n"
      puts $fp "   \}\n\}"
      close $fp
   } else {
      puts "Something wrong"
   }
}

catch {
   rename shape2fid shape2fidinternal
   proc shape2fid {shp dt args} {
      set f [eval [concat shape2fidinternal $shp $dt $args]]
      set d [dict create version "1.0" dimensions {0 {type linear quantity time}} dependent_variables {0 {type internal numeric_type complex64 quantity_type scalar}}]
      dict set d dimensions 0 count [fget $f -np]
      dict set d dimensions 0 increment "$dt s"
      set ::csdm($f) $d
      puts [fget $f -np]
      return $f
   }
} dummy

